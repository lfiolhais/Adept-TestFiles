#include "init.S"

  li a1, -10
  li a2, 20

  blt  a2, a1, exit  //a2>a1 so it will not branch
  addi a3, a1, 30    //This instruction will be executed
  blt  a3, a2, exit  //a3=a2 so ti will not branch
  addi a4, a2, -35   //This instruction will be executed and a4=-15
  blt  a4, a1, exit  //a4<a1 so it will jump to exit
  addi a5, a5, 1     //a5=0 because this instruction isn't execute

exit:
#include "exit.S"
