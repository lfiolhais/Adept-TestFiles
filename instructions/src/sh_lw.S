#include "init.S"

  li a0, 100
  li a1, 0xF168
  li a2, 0xF122

  sh a2, 28(a0)    // M[a0+28] = a2
  sh a1, 30(a0)    // M[a0+30] = a1

  lw a3, 28(a0)    // a3 = M[a0+28], so a3=0xF168F122

#include "exit.S"
