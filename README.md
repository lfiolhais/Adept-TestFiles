# Test Programs
This repository contains several C programs to test a RISC-V processor.
To generate hex files for all programs, run `make hex`.
